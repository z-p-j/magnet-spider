# MagnetSpider

<img src="entry/src/main/resources/base/media/icon.png" width="128px" />

## 项目介绍

简单的磁力搜索app，实现了一个非常简单的网页爬虫，解析html数据并展示，并简单实现了加载更多。其中简单实现了通过css选择器查找html dom元素。

## 开发环境

- DevEco Studio 3.1 Beta1
- SDK API9 3.2.10.6 Beta5

## 截图预览

<img src="screenshot/1.jpg" width="300px" />
<img src="screenshot/2.jpg" width="300px" />