
export interface MagnetData {

    magnetLink: string;
    title: string;
    info: string;
    isDir: number;

}